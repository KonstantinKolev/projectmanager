import { Link, Route, Routes } from 'react-router-dom';
import './App.css';
import AuthService from './services/auth.service';
import "bootstrap/dist/css/bootstrap.min.css";
import { useEffect, useState } from 'react';
import Home from './components/Home';
import Login from './components/Login';
import Profile from './components/Profile';
import Register from './components/Register';
import Projects from './components/Projects';

const App = () => {
  const [currentUser, setCurrentUser] = useState(undefined);

  useEffect(() => {
    const user = AuthService.getCurrentUser();

    if (user) {
      setCurrentUser(user);
    }
  }, []);

  const logOut = () => {
    AuthService.logout();
    setCurrentUser(undefined);
  };

  return (
    <div>
      <nav className='navbar navbar-expand navbar-dark bg-dark'>
        <Link to={'/'} className='navbar-brand'>
          ProjectManager
        </Link>
        <div className='navbar-nav mr-auto'>
          <li className='nav-item'>
            <Link to={'/home'} className='nav-link'>
              Home
            </Link>
          </li>

          {currentUser && (
            <>
            <li className='nav-item'>
              <Link to={'/user'} className='nav-link'>
                User
              </Link>
            </li>
            <li className='nav-item'>
              <Link to={'/projects'} className='nav-link'>
                Projects
              </Link>
            </li>
            </>
          )}
        </div>
        {currentUser ? (
          <div className='navbar-nav ml-auto'>
            <li className='nav-item'>
              <Link to={'/profile'} className='nav-link'>
                {currentUser.name}
              </Link>
            </li>
            <li className='nav-item'>
              <a href='login' className='nav-link' onClick={logOut}>
                LogOut
              </a>
            </li>
          </div>
        ) : (
          <div className='navbar-nav ml-auto'>
            <li className='nav-item'>
              <Link to={'/login'} className='nav-link'>
                Login
              </Link>
            </li>

            <li className='nav-item'>
              <Link to={'/register'} className='nav-link'>
                Sign Up
              </Link>
            </li>
          </div>
        )}
      </nav>

      <div className='container mt-3'>
          <Routes>
            <Route path='/home' Component={Home} />
            <Route exact path='/' Component={Home} />
            <Route exact path='/login' Component={Login} />
            <Route exact path='/register' Component={Register} />
            <Route exact path='/user' Component={Profile} />
            <Route exact path='/projects' Component={Projects} />
          </Routes>
      </div>
    </div>
  );
}

export default App;