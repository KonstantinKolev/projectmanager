import api from "./api";
import jwtDecode from "jwt-decode";
import TokenService from "./token.service";

const login = (email, password) => {
    return api
        .post(`/tokens/generate?email=${email}&password=${password}`)
        .then(response => {
            if (response.data.accessToken) {
                const decoded = jwtDecode(response.data.accessToken);
                TokenService.setUser({
                    id: decoded.id,
                    name: decoded.name,
                    accessToken: response.data.accessToken,
                    refreshToken: response.data.refreshToken,
                });
            }

            return response.data;
        });
};
const logout = () => {
    TokenService.removeUser();
};
const register = (name, email, password) => {
    return api.post("/users", {
        name,
        email,
        password
    });
};
const getCurrentUser = () => {
    return JSON.parse(localStorage.getItem('user'));
};

const AuthService = {
    login,
    logout,
    register,
    getCurrentUser,
};

export default AuthService;
