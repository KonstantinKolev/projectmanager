import api from './api';

const getAllProjects = () => {
    return api.get('/projects');
};

const getProjectById = (id) => {
    return api.get('/projects/' + id);
};

const createProject = (title, description, creatorId) => {
    return api.post('/projects', {
        title,
        description,
        creatorId,
    });
};

const ProjectService = {
    getAllProjects,
    getProjectById,
    createProject,
};

export default ProjectService;