import axios from "axios";
import TokenService from "./token.service";

const instance = axios.create({
    baseURL: 'https://localhost:44346/api',
    headers: {
        'Content-Type': 'application/json',
    },
});

instance.interceptors.request.use(
    (config) => {
        const token = TokenService.getLocalAccessToken();
        if (token) {
            config.headers['Authorization'] = 'Bearer ' + token;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

instance.interceptors.response.use(
    (res) => {
        return res;
    },
    async (err) => {
        const originalConfig = err.config;

        if (originalConfig.url !== 'tokens/generate' && err.response) {
            if (err.response.status === 401 && !originalConfig._retry) {
                originalConfig._retry = true;

                try {
                    const rs = await instance.post('/tokens/refresh', {
                        refreshToken: TokenService.getLocalRefreshToken(),
                    });

                    const { accessToken, refreshToken } = rs.data;
                    TokenService.updateLocalAccessToken(accessToken);
                    TokenService.updateLocalRefreshToken(refreshToken);

                    return instance(originalConfig);
                } catch (_error) {
                    return Promise.reject(_error);
                }
            }
        }

        return Promise.reject(err);
    }
);

export default instance;