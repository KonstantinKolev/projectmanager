import { useState } from "react";
import { useEffect } from "react";
import ProjectService from "../services/project.service";
import { Table, TableBody, TableCell, TableFooter, TableHead, TablePagination, TableRow, TextField } from "@mui/material";

const Projects = () => {
    const [data, setData] = useState([]);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [titleFilter, setTitleFilter] = useState('');
    const [creatorFilter, setCreatorFilter] = useState('');

    useEffect(() => {
        ProjectService.getAllProjects().then(
            (response) => {
                setData(response.data);
            }
        )
    }, []);

    const handlePageChange = (event, newPage) => {
        setPage(newPage);
    }

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    }

    const handleTitleFitlerChange = (e) => {
        setTitleFilter(e.target.value);
    };

    const handleCreatorFilterChange = (e) => {
        setCreatorFilter(e.target.value);
    };

    const filteredData = data.filter(item => item.title.includes(titleFilter) && item.creatorName.includes(creatorFilter));

    return (
        <>
            <TextField label="Title" variant="outlined" value={titleFilter} onChange={handleTitleFitlerChange} />
            <TextField label='Creator' variant="outlined" value={creatorFilter} onChange={handleCreatorFilterChange} />
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Id</TableCell>
                        <TableCell>Project Name</TableCell>
                        <TableCell>Creator</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {(rowsPerPage > 0
                        ? filteredData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : filteredData
                    ).map((item) => {
                        return (
                            <TableRow key={item.id}>
                                <TableCell>{item.id}</TableCell>
                                <TableCell>{item.title}</TableCell>
                                <TableCell>{item.creatorName}</TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TablePagination
                            rowsPerPageOptions={[1, 3, 5, 10, { label: 'All', value: -1 }]}
                            count={filteredData.length}
                            page={page}
                            rowsPerPage={rowsPerPage}
                            onPageChange={handlePageChange}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                    </TableRow>
                </TableFooter>
            </Table>
        </>
    );
};

export default Projects;