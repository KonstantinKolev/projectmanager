﻿using Microsoft.IdentityModel.Tokens;
using ProjectService.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Web;

namespace ProjectService.Services
{
    public static class TokenGenerator
    {
        public static string GenerateTokenForUser(User user)
        {
            string key = "Secret_Key_369_asd@test_61430#asdfuipods";
            var issuer = "https://localhost:44346";
            var audience = "https://localhost:3000";

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var permClaims = new List<Claim>();
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            permClaims.Add(new Claim("name", user.Name));
            permClaims.Add(new Claim("id", user.Id.ToString()));

            var token = new JwtSecurityToken(issuer,
                audience,
                permClaims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: credentials);
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt_token;
        }
    }
}