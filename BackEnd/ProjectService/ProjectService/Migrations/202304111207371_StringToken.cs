﻿namespace ProjectService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StringToken : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RefreshTokens", "Token", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RefreshTokens", "Token", c => c.Guid(nullable: false));
        }
    }
}
