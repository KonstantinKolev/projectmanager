﻿namespace ProjectService.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using ProjectService.Models;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProjectService.Data.ProjectServiceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProjectService.Data.ProjectServiceContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            context.Users.AddOrUpdate(x => x.Id,
                new User() { Id = 1, Name = "Admin", Email = "admin@mail.com", Password = "adminpass" }
                );
        }
    }
}
