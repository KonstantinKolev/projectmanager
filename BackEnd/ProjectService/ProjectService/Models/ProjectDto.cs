﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectService.Models
{
    public class ProjectDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string CreatorName { get; set; }
    }
}