﻿using ProjectService.Data;
using ProjectService.Models;
using ProjectService.Services;
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProjectService.Controllers
{
    [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
    public class TokensController : ApiController
    {
        private ProjectServiceContext _context = new ProjectServiceContext();

        [HttpPost]
        public async Task<IHttpActionResult> Generate(string email, string password)
        {
            User user = await _context.Users.FirstOrDefaultAsync(u => u.Email == email && u.Password == password);
            if (user == null)
            {
                return BadRequest(ModelState);
            }

            var jwtToken = TokenGenerator.GenerateTokenForUser(user);

            var existingRefresshTokens = _context.RefreshTokens.Where(t => !t.Used && t.UserId==user.Id).ToList();
            foreach( var existingToken in existingRefresshTokens)
            {
                existingToken.Used = true;
                _context.RefreshTokens.AddOrUpdate(existingToken);
            }

            var refreshToken = new RefreshToken()
            {
                Token = Guid.NewGuid().ToString(),
                UserId = user.Id,
                Used = false
            };
            _context.RefreshTokens.Add(refreshToken);
            await _context.SaveChangesAsync();

            return Ok(new { accessToken = jwtToken, refreshToken = refreshToken.Token });

        }

        [HttpPost]
        public async Task<IHttpActionResult> Refresh([FromBody] string refrehToken)
        {
            RefreshToken refreshTokenObject = await _context.RefreshTokens.FirstOrDefaultAsync(t => t.Token == refrehToken && !t.Used);
            if (refreshTokenObject == null)
            {
                return BadRequest(ModelState);
            }

            User user = await _context.Users.FirstOrDefaultAsync(u => u.Id == refreshTokenObject.UserId);
            var jwtToken = TokenGenerator.GenerateTokenForUser(user);

            refreshTokenObject.Used = true;
            _context.RefreshTokens.AddOrUpdate(refreshTokenObject);

            var newRefreshToken = new RefreshToken()
            {
                Token = Guid.NewGuid().ToString(),
                Used = false,
                UserId = user.Id
            };
            _context.RefreshTokens.Add(newRefreshToken);
            await _context.SaveChangesAsync();

            return Ok(new { accessToken = jwtToken, refrehToken = newRefreshToken.Token });
        }
    }
}
