# ProjectManager



## Getting started

### Back End Server
To start the back end server just open the .sln file and start it from VS.

### Front End
For the front end project go to FrontEnd/project-manager folder and run `npm install`. After that to start it just run `npm start`.

### Seeded account
Email: admin@mail.com 
Password: adminpass
